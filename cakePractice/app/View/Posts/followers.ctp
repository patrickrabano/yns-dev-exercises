<?php $logId = AuthComponent::user('id'); ?>

<div class="users view">
	<?php if(count($followers) != 0 ){ ?>
	<h2 style="margin-top:20px;align:center;"><?= h(ucfirst($followers[0]['UserFrom']['first_name'])). "'s " . "Followers" ?></h2>	
		<?php foreach($followers as $follower => $follow): ?>
			<?php if($follow['Follow']['status'] == 1): ?>
				<div style="width:450px; border: 3px solid black; padding: 5px; margin-bottom:25px; margin-right:15px; ">
					<dl style="float:right; width: 70%;">				
						<dt><?= 'Name:' ?></dt>
						<dd>
							<?= 
							$this->Html->link(__(h($follow['UserTo']['first_name'] . ' ' . $follow['UserTo']['last_name'] )),
							[
								'controller'=>'users',
								'action' => 'view', $follow['UserTo']['id']
							]);
							?>
							&nbsp;
						<dt><?= __('Follower Since:'); ?></dt>
						<dd>
							<?= h($follow['Follow']['date_followed']); ?>
						</dd>
					</dl>	
					<?= $this->Html->image('/uploads/' . h($follow['UserTo']['image']), ['height' => '100px' , 'width' => '130px']) ?>
				</div>
		<?php endif; endforeach; ?>
		<?php } ?>
								
		<?php if(count($following)!= 0){ ?>
		<h2><?= h(ucfirst($following[0]['UserTo']['first_name'])). "'s " . "Following" ?></h2>
			<?php foreach($following as $follows => $userFol):?>
				<?php if($userFol['Follow']['status'] == 1): ?>
					<div style="width:500px; border: 3px solid black; padding: 5px; margin-bottom:10px;">
						<dl style="float:right; width: 70%;">		
							<dt><?= 'Name:' ?></dt>
							<dd>
								<?= 
								$this->Html->link(__(h($userFol['UserFrom']['first_name'] . ' ' . $userFol['UserFrom']['last_name'])), 
								[
									'controller'=>'users',
									'action' => 'view', $userFol['UserFrom']['id']
								]);
								?>
								&nbsp;
							<dt><?= __('Follower Since:'); ?></dt>
							<dd>
								<?= h($userFol['Follow']['date_followed']); ?>
							</dd>
						</dl>	
						<?= $this->Html->image('/uploads/' . h($userFol['UserFrom']['image']), ['height' => '100px' , 'width' => '140px']) ?>
					</div>
			<?php endif; endforeach; ?>
			<?php } ?>
		</div>
		
		<div class="actions">
			<div style="text-align: center;">
				<h3><?= ucfirst($user['User']['first_name']) . " " .  $user['User']['last_name'] ?></h3>	
				<?= $this->Html->image('/uploads/' . $user['User']['image'], ['height' => '150px' , 'width' => '180px']) ?>
			</div>			
			
			<ul>
				<li><?= $this->Html->link(__('Home'), array('controller'=>'Posts','action' => 'index')); ?> </li>
				<li><?= $this->Html->link(__('Add new post'), array('controller'=>'Posts','action' => 'add')); ?> </li>
				<li><?= $this->Html->link(__('Edit Profile'), array('controller' => 'users', 'action' => 'edit', $logId)); ?></li>
				<li><?= $this->Html->link(__('Logout'), array('controller' => 'users', 'action' => 'logout')); ?></li>
			</ul>
		</div>
	
