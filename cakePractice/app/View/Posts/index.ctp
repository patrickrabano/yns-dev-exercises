<div class="posts index">
	<?php
		 
		  $logFirst = AuthComponent::user('first_name');
		  $logLast = AuthComponent::user('last_name'); 
		  $logId = AuthComponent::user('id');

		
	?>

	
	<h2><?= __('Blog Posts'); ?></h2>
	



	<?php 
	
	echo $this->Form->create('User',['url' => 'search']); 
	echo $this->Form->input('keyword',['style'=>'width:400px; height:10px;','class'=>'form-control input-sm','maxlength'=>"64",'placeholder'=>' Search Here..','div'=>false,'label'=>false]); 
	echo $this->Form->submit('Search');
	echo $this->Form->end(); 
	
	
	?>
	
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			
			<th style="text-align:center !important;"><?= $this->Paginator->sort('title'); ?></th>
			<th style="text-align:center !important;"><?= $this->Paginator->sort('posted by'); ?></th>
			<th style="text-align:center !important;"><?= $this->Paginator->sort('created'); ?></th>
			<th style="text-align:center !important;"><?= $this->Paginator->sort('modified'); ?></th>
			<th style="text-align:center !important;" class="actions"><?= __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	
	<?php foreach ($posts as $post):
		 ?>
	<tr>
		
		<td style="text-align:center !important;"><?= $this->Html->link(__(h($post['Post']['title'])), ['action' => 'view', $post['Post']['id']]); ?>&nbsp;</td>
		<td style="text-align:center;">
		<?=  

		$this->Html->image('/uploads/' . h($post['User']['image']), 
		['height' => '50px' , 
			'width' => '70px', 
				"alt" => $post['User']['first_name'] . " " . $post['User']['last_name'],
		 
		 'url' => [
			 'controller' => 'users',
			 	'action' => 'view',$post['User']['id']
		 		]
		 	]
		 ) . "<br>" . h(ucfirst($post['User']['first_name']))." ". h($post['User']['last_name']);   ?></td>
		
		<td style="text-align:center !important;"><?= h($post['Post']['created']); ?>&nbsp;</td>
		<td style="text-align:center !important;"><?= h($post['Post']['modified']); ?>&nbsp;</td>
		<td class="actions">

			<?= $this->Html->link(__('View'), array('action' => 'view', $post['Post']['id']));
			echo $this->Html->link(__('Share'), array('action' => 'share', $post['Post']['id']));	

		
					if($logId === $post['User']['id']):
					
						echo $this->Html->link(__('Edit'), array('action' => 'edit', $post['Post']['id'])); 
						echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $post['Post']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $post['Post']['id']))); ?>

		</td>
	</tr>
			<?php endif; endforeach; ?>
		
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ' '));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3 style="text-align:center;"><?= 'Welcome!'?></h3>
	<h3 style="text-align:center;"><?=  h(ucfirst($logFirst)) . " " . h($logLast) ?></h3> 
	<div style="margin-left: 15px;">
	<?= $this->Html->image('/uploads/' . h($user['image']), ['height' => '150px' , 'width' => '180px'])?>
	</div>
	
	<div>
	<br>
		<ul>

			<li><?= $this->Html->link(__('Home'), array('action' => 'index')); ?></li>
			<li><?= $this->Html->link(__('Add new post'), array('action' => 'add')); ?></li>
			<li><?= $this->Html->link(__('View Followers'), array('controller' => 'posts', 'action' => 'followers',$logId)); ?></li>
			<li><?= $this->Html->link(__('View Your posts'), array('controller'=>'users','action' => 'view',$logId)); ?></li>
			<li><?= $this->Html->link(__('Edit Profile'), array('controller' => 'users', 'action' => 'edit', $logId)); ?></li>
			<li><?= $this->Html->link(__('Logout'), array('controller' => 'users', 'action' => 'logout')); ?></li>
		
		</ul>
	</div>	
</div>
