<style>
#c1 {
    float:left;
	width:550px; border: 3px solid black; padding: 10px; margin-bottom:25px;
    
}
#c2 {
    float:left;
	width:300px; border: 3px solid black; padding: 10px; margin-bottom:25px;
    
}
</style>


<?php 
$logId = AuthComponent::user('id') ;

?>

<div class="posts view">
<h2><?= 'Post' ?></h2>
	<dl>
		<dt><?= 'Posted By' ?></dt>
		<dd>
			<?= $this->Html->link(__(h(ucfirst($post['User']['first_name']) . " " . h($post['User']['last_name']))), 
			[
				'controller'=>'users',
				'action' => 'view', $post['User']['id']
			]); 
			?>
			&nbsp;
		</dd>
		<dt><?= 'Title' ?></dt>
		<dd>
			<?= h($post['Post']['title']); ?>
			&nbsp;
		</dd>
		<dt><?= 'Body' ?></dt>
		<dd>
			<?= h($post['Post']['body']); ?>
			&nbsp;
		</dd>
		<dt><?= 'Created' ?></dt>
		<dd>
			<?= h($post['Post']['created']); ?>
			&nbsp;
		</dd>
		<dt><?= 'Modified' ?></dt>
		<dd>
			<?= h($post['Post']['modified']); ?>
			&nbsp;
		</dd>
	<br>


	

	<?php foreach($post['Image'] as $images): 
		    if($images['deleted'] == 0):?>

		<div style="display: inline-block;">
		<?= $this->Html->image('/uploads/' . h( $images['filename']), ['height' => '150px' , 'width' => '180px'])?>
		</div>
	<?php endif; endforeach; ?>

	</dl>
		<br>



		<?php if(count($share) != 0): ?>
		
		<h2><?= __('Shared Post'); ?></h2>
		<div id="c1">
		<?= $this->Html->image('/uploads/' . h($share['User']['image']), ['height' => '110px' , 'width' => '130px'])?>
			<dl style="width: 75%; float:right !important;">
			
				<dt><?= 'Posted By' ?></dt>
				<dd>
				<?= $this->Html->link(__(h(ucfirst($share['User']['first_name']) . " " . $share['User']['last_name'])), 
				[
					'controller'=>'users',
					'action' => 'view', $share['User']['id']
				]); 
				?>	
				</dd>
				<dt><?= 'Title' ?></dt>
				<dd>
					<?= $this->Html->link(__(h($share['Post']['title'])), ['controller'=>'posts','action' => 'view', $share['Post']['id']]); ?>	
				</dd>
				<dt><?= 'Body' ?></dt>
				<dd>
					<?= h($share['Post']['body']); ?>		
				</dd>		
				<br>
			</dl>
			</div>
		<?php endif; ?>

	<?php foreach($post['Like'] as $likes){
	
		if($logId == $likes['user_id'] && $likes['deleted'] == 1){ ?>
		

		<div class="actions">
			<li><?= $this->Html->link(__('Like Post'), ['action' => 'like' , $post['Post']['id'],$logId]); ?> </li>					
		</div>
						
		<?php } 
	
		 if($logId == $likes['user_id'] && $likes['deleted'] == 0){ ?>

		<div class="actions">
			<li><?= $this->Html->link(__('Unlike Post'), ['action' => 'unlike' , $post['Post']['id'],$logId]); ?> </li>		
		</div>

			<?php } ?>
		<?php } ?>
		
		<br>

	<?php

	
	echo $this->Form->create('Comment'); 
	echo $this->Form->input('message',['class'=>'form-control input-sm','placeholder'=>'Enter Comment Here','div'=>false,'label'=>false]); 
	echo $this->Form->submit('Submit');
	echo $this->Form->end(); 
			
	?>
	<div>	
		<h5>Comments :</h5>
		<div class="actions">
		
		<?php foreach($post['Comment'] as $comments): ?>
		<?php if($comments['deleted'] != 1): ?>
		<div id="c2"> 
			<dl>
				<dt style=" width:300px;">	
				
					<?= h($comments['message']) . " " . "By" . " " . h($comments['username'])?>
					
					<?php if($logId == $comments['user_id']):?>				
					
						<li><?= $this->Html->link(__('Edit'), ['action' => 'editcomment', $comments['id']]); ?> </li>
				
						<li><?= $this->Html->link(__('Delete'), ['action' => 'deletecomment',$comments['id']],['confirm' => __('Are you sure you want to delete comment?')]); ?> </li>
					
					<?php  endif; ?>	
				</dt>
			</dl>		
		 	
		</div>
		<?php endif; ?>
		<?php endforeach;?>
		</div>
	</div>
</div>



<div class="actions">
	<h3><?= __('Actions'); ?></h3>
	<ul>
		<li><?= $this->Html->link(__('Home'), ['action' => 'index']); ?> </li>
			<?php if($logId === $post['User']['id']): ?>
				<li><?= $this->Html->link(__('Edit Post'), array('action' => 'edit', $post['Post']['id'])); ?> </li>
				<li><?= $this->Form->postLink(__('Delete Post'), array('action' => 'delete', $post['Post']['id']), ['confirm' => __('Are you sure you want to delete # %s?', $post['Post']['id'])]); ?> </li>					
			<?php endif; ?>
		<li><?= $this->Html->link(__('Add new post'), array('action' => 'add')); ?> </li>
		<li><?= $this->Html->link(__('Logout'), array('controller' => 'users', 'action' => 'logout')); ?></li>
	</ul>
</div>
