<?php
        if(empty($_POST['Fname']) || empty($_POST['Lname'])){

            echo "Please Input First Name and Last Name!<br>";
        }

        elseif(!is_numeric($_POST['age'])){
            echo "Please Input Valid Age!<br>";
        }

        elseif($_POST['Email'] != filter_var($_POST['Email'], FILTER_VALIDATE_EMAIL)){
            echo "Invalid Email!";
        }

        elseif(empty($_POST['gender'])){
            echo "Please Select One Gender!"; 
        }

        elseif($_POST['gender'] == "male"){
            echo "Hello MR. ".$_POST['Fname']." ".$_POST['Lname']."<br>"
            . "Your Email is:" .$_POST['Email'];
        }
        
        else{
            echo "Hello MRS./MS. ".$_POST['Fname']." ".$_POST['Lname']."<br>"
            . "Your Email is: " .$_POST['Email'];
        }
        