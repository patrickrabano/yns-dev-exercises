<?php
App::uses('AppModel', 'Model');

/**
 * Post Model
 *
 */
class Post extends AppModel {

	public $belongsTo = [
        'User' => [
            'className' => 'User',
            'foreignKey' => 'user_id'
			],

		
			
		];

		public $hasMany = [
			'Comment' => [
				'className' => 'Comment',
				'foreignKey' => 'post_id'
			],

			
			'Like' =>[
					'className' => 'Like',
					'foreignKey' => 'post_id'
			],
		
			'Image'=>[

					'className' => 'Image',
					'foreignKey' => 'post_id'

				],



			];

		

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'title';

/**
 * Validation rules
 *
 * @var array
 * 
*/
		public function isOwnedBy($post, $user) {
			return $this->field('id', array('id' => $post, 'user_id' => $user)) !== false;
		}


		public $validate = array(

			'id' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'title' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'body' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),

		// 'post_pic' => [
		// 	'rule' => [
			
		// 		'extension',['jpeg', 'png', 'jpg']],				
		// 		'message' => 'Please supply a valid image.',
		// 	],

		
	);
}
