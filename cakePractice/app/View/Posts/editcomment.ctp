<div class="posts form">	
<?php echo $this->Form->create('Comment'); ?>
	
	<fieldset>
		<legend><?php echo __('Edit Comment'); ?></legend>
	<?php
		echo $this->Form->input('message', ['style'=>'width:400px; height:20px;']); ?>
	</fieldset>
	
	<?php echo $this->Form->end(__('Submit')); ?>
	
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Home'), ['action' => 'index']); ?></li>
		<li><?php echo $this->Html->link(__('Add New Post'), ['action' => 'add']); ?></li>
		<li><?php echo $this->Form->postLink(__('Delete Comment'),
		 	['action' => 'delete',$this->Form->value('Post.id')], 
		  	['confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('Post.id'))]); ?></li>		
	</ul>
</div>
