<?php

App::uses('AppModel', 'Model');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');

class User extends AppModel {

     public $hasAndBelongsToMany = [
        'userFollow' => [
            'className' => 'User',
            'joinTable' => 'follows',
            'foreignKey' => 'user_id',
            'associationForeignKey' => 'follow_user_id'
        ],

        'userFollowing' => [
            'className' => 'User',
            'joinTable' => 'follows',
            'foreignKey' => 'follow_user_id',
            'associationForeignKey' => 'user_id'
        ],

    ];

    public $hasMany = [

        'likes'=>[

           'className' => 'Like'

        ],


        'comments' => [
            'className' => 'Comment',
            'foreignKey' => 'user_id'
       
        ], 

        'posts' => [
            'className' => 'Posts',
       
        ],

            'followFrom'=>[
                'className'=>'Follow',
                'foreignKey'=>'user_id'
             ],
             
             'followTo'=>[
                'className'=>'Follow',
                'foreignKey'=>'follow_user_id'
             ]
        ];


    public function beforeSave($options = []) {
        if (isset($this->data[$this->alias]['password'])) {
            $passwordHasher = new BlowfishPasswordHasher();
            $this->data[$this->alias]['password'] = $passwordHasher->hash(
                $this->data[$this->alias]['password']
            );
        }
        return true;
    }

    public $validate = array(

        'email' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'Email is Required'
            ),
            'unique' => array(
                'rule'    => 'isUnique',
                'message' => 'This Email is already taken'
            ),
        ),

        'first_name' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'First Name Required'
            ),

            'alphaNumeric' => array(
                'rule' => 'alphaNumeric',
                'required' => true,
                'message' => 'Letters and numbers only'
            ),

        ),

        'last_name' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'Last Name Required'
            ),

            'alphaNumeric' => array(
                'rule' => 'alphaNumeric',
                'required' => true,
                'message' => 'Letters and numbers only'
            ),
        ),
        


        'username' => array(
            'notBlank' => [
                'rule' => array('notBlank'),
                'message' => 'A username is required',
                'allowEmpty' => false
            ],
            'between' => array( 
                'rule' => array('between', 3, 15), 
                'required' => true, 
                'message' => 'Usernames must be between 3 to 15 characters'
            ),
             'unique' => array(
                'rule'    => 'isUnique',
                'message' => 'This Username is already taken'
            ),
        ),


        'password' => array(
            'notBlank' => array(
                   'rule' => array('notBlank'),
                   'message' => 'Please enter your password.',
            ),
           'matchPasswords' => array(
                   'required' => true,
                   'rule' => array('matchPasswords'),                   
                   'message' => 'Your passwords do not match!'
            )
        )

    );

    public function matchPasswords($data){
        if ($data['password']==$this->data['User']['confirm_password']){
              return true;
         }
         $this->invalidate('confirm_password','Your passwords do not match!');
          return false;
  }

}
