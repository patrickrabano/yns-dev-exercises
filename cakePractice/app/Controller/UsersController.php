<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class UsersController extends AppController {

    
    // public function follow() {
    //     $data = $this->User->followFrom->find('all', 
    //       [   
    //         'contain'=> ['follow_user_id']
    //       ]
    //     );
   
    //   }

    //   public function getFollow($idToFind) {
    //     $data = $this->followFrom->find('all', 
    //       array(
    //         'conditions'=>array(
    //           'OR'=> array(
    //               array('user_id' => $idToFind),
    //               array('follow_user_id' => $idToFind)
    //           )
    //           )
    //       )
    //     );

    //     $followers = [];
    //     foreach ($data as $i) {
    //       if ($i['followFrom']['user_id'] == $idToFind){
    //           $followers[] = $i['follow_user_id'];
    //       }
    //       elseif ($i['followFrom']['follow_user_id'] == $idToFind){
    //           $followers[] = $i['user_id'];
    //       }
    //     }
    //   }


    public function index() {
        $this->User->recursive = 0;
        $this->set('users', $this->paginate());
    }

    public function view($id = null , $userId = null) {
        $this->loadModel('Post');
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        $this->set(['user' => $this->User->findById($id)]);
    }

    public function activation($token = null){

        $this->User->updateAll(['account_status' => '1'], ['token' => $token]);
        $this->Flash->success(__('Your Account has been activated!'));
        $this->redirect($this->Auth->logout());

    }

    public function add() {
        if ($this->request->is('post')) {
			$this->User->create();

			$token  = md5(Security::hash(Security::randomBytes(32)));
            $this->request->data['User']['token'] = $token;
            $userEmail = $this->request->data['User']['email'];

            if ($this->User->save($this->request->data)) {
                $this->Flash->success(__('Success! Please Check your email for Activation!'));
                
                $email = new CakeEmail('gmail');
                $email->emailFormat('html');
                $email->from(['microblog@yns.com' => 'Account Activation']);
                $email->to($userEmail);
                $email->subject('Activation Account');
                $email->send('Please Click The link To activate your Account!<br><a href="http://dev2.ynsdev.pw/cakePractice/users/activation/' . $token . '">Activate Account</a>');
                               
                return $this->redirect(['action' => 'login']);
            }


            $this->Flash->error(
                __('Error! Please Try again')
            );
        }
    }

    public function edit() {

         
        $this->User->id = $this->Auth->user('id');
        if (!$this->User->exists()) {

            throw new NotFoundException(__('Invalid user'));
        }

        if ($this->request->is('post') || $this->request->is('put')) {

            $image = $this->request->data['User']['image'];
            $uploaded = $image['tmp_name'];
            $fileName = md5(Security::hash($uploaded.Security::randomBytes(32)));
            $location = 'uploads/' .  $fileName;

            $extension = explode('.',$image['name']);
            
            $fileType =  strtolower(end($extension));

            $this->request->data['User']['image'] = $fileName . "." . $fileType; 
         
            if ($this->User->save($this->request->data)) {
               
                
                move_uploaded_file($uploaded, WWW_ROOT . $location . "." . $fileType);
                                 
                $this->Flash->success(__('Success! Profile Has been Updated!'));
                return $this->redirect(['controller'=>'posts', 'action' => 'index']);
            }
            $this->Flash->error(
                __('The user could not be saved. Please, try again.')
            );
        } else {
            $this->request->data = $this->User->findById($this->Auth->user('id'));
            unset($this->request->data['User']['password']);
        }

        
    }

    public function delete() {
       
        $this->User->id = $this->Auth->user('id');
        $this->request->allowMethod('post');

        
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->User->delete()) {
            $this->Flash->success(__('User deleted'));
            return $this->redirect(array('action' => 'index'));
        }
        $this->Flash->error(__('User was not deleted'));
        return $this->redirect(array('action' => 'index'));
	}
	
	public function beforeFilter() {
		parent::beforeFilter();
	
		$this->Auth->allow('add', 'logout');
	}

		public function login() {
			if ($this->request->is('post')) {
				if ($this->Auth->login()) {
					
					if($this->Auth->user('account_status') == 0){

						$this->Flash->error(__('Please activate your account first!'));
						
						return $this->redirect(['action' => 'login']);
				    }
					return $this->redirect($this->Auth->redirectUrl());
				}
				$this->Flash->error(__('Invalid username or password, try again'));
			}
		}
		
		public function logout() {
			return $this->redirect($this->Auth->logout());
        }
        



        public function isAuthorized($user) {
           
            
           if ($this->action === 'edit' || $this->action === 'activation') {

                return true;
            }
        
            
            if (in_array($this->action, array('edit', 'delete', 'activation'))) {
                $postId = (int) $this->request->params['pass'][0];
                if ($this->Post->isOwnedBy($postId, $user['id'])) {
                    return true;
                }

                if ($this->User->isOwnedBy($user['id'], $user['id'])) {
                    return true;
                }

            }
        
            return parent::isAuthorized($user);
        }



}