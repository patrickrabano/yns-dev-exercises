<div class="posts index">
	<?php
		 
		  $logFirst = AuthComponent::user('first_name');
		  $logLast = AuthComponent::user('last_name'); 
		  $logId = AuthComponent::user('id');

		
	?>

	<h2><?php echo __('Blog Posts'); ?></h2>
	
	<?php 
	
	echo $this->Form->create('User',array('class'=>'searchForm','role'=>'search','autocomplete'=>'off')); 
	echo $this->Form->input('keyword',array('class'=>'form-control input-sm','maxlength'=>"64",'placeholder'=>' Search Here...','div'=>false,'label'=>false)); 
	echo $this->Form->submit('Search');
	echo $this->Form->end(); 
	
	
	
	?>
		
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			
			<th><?php echo $this->Paginator->sort('First Name'); ?></th>
			<th><?php echo $this->Paginator->sort('username'); ?></th>
			<th><?php echo $this->Paginator->sort('email'); ?></th>
			<th><?php echo $this->Paginator->sort('Followers'); ?></th>
	</tr>
	</thead>
	<tbody>

	<?php foreach($data as $results): ?>


	<tr>
		<td><?php echo h($results['User']['first_name']); ?>&nbsp;</td>
		<td><?php echo h($results['User']['username']); ?>&nbsp;</td>
		<td><?php echo h($results['User']['email']); ?>&nbsp;</td>
		<td><?php echo h(count($results['followFrom'])); ?>&nbsp;</td>
	</tr>

	<?php endforeach; ?>

	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3 style="text-align:center;"><?= 'Welcome!'?></h3>
	<h3 style="text-align:center;"><?=  ucfirst($logFirst) . " " .$logLast ?></h3> 
	<div style="margin-left: 15px;">
	<?= $this->Html->image('/uploads/' . $user['image'], ['height' => '150px' , 'width' => '180px'])?>
	</div>
	
	<div>
	<br>
		<ul>
            <li><?php echo $this->Html->link(__('Home'), array('action' => 'index')); ?></li> 
			<li><?php echo $this->Html->link(__('View Followers'), array('controller' => 'posts', 'action' => 'followers',$logId)); ?></li>
			<li><?php echo $this->Html->link(__('Add new post'), array('action' => 'add')); ?></li>
			<li><?php echo $this->Html->link(__('View Your posts'), array('controller'=>'users','action' => 'view',$logId)); ?></li>
			<li><?php echo $this->Html->link(__('Edit Profile'), array('controller' => 'users', 'action' => 'edit', $logId)); ?></li>
			<li><?php echo $this->Html->link(__('Logout'), array('controller' => 'users', 'action' => 'logout')); ?></li>
		
		</ul>
	</div>	
</div>
