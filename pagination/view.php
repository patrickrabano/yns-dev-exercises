<!DOCTYPE html>

<br><?= "Number of Records per Page " . $csv->show?>
<br><?= "Total Records ". count($data)?>
<br><?= "Page: " . $csv->page?>
<br>

<table class="table table-bordered table-striped">
  <thead>
    <tr>
      <th>First Name</th>
      <th>Last Name</th>
      <th>Email</th>
      <th>Age</th>
      <th>Gender</th>
      <th>Image</th>
    </tr>
  </thead>
<tbody>

<?php
//  var_dump($csv->csv);die;

foreach($csv->csv as $d => $c):
  
    foreach($c as $b ): 
      
         $details =  explode("," , $b);
      // var_dump($c);die;
?>

      <tr>
        <td><?= $details[0]?></td>
        <td><?= $details[1]?></td>
        <td><?= $details[2]?></td>
        <td><?= $details[3]?></td>
        <td><?= $details[4]?></td>
        <td><img style="height: 70px; width: 70px;" src="../images/<?=$details[5]?>"></td>
      </tr>

<?php
    endforeach; 
  endforeach;?>
    </tbody>
</table>
<?php if(count($csv->pager) > 0):?>

<ul>
    <?php if($csv->prev == ''):?>
        <li class="disabled"><a href="#">prev</a></li>
            <?php else:?>
                <li><a href="?p=<?= urlencode($csv->prev)?>">prev</a></li>
    <?php endif;?>

  <?php foreach($csv->pager as $p):?> 
      
      <li><?=($p == $csv->current)?' ':'';?>><a href="?p=<?= urlencode($p)?>"><?= htmlspecialchars($p)?></a></li>
  
  <?php endforeach;?>

      <?php if($csv->next == ''):?>
        <li class="disabled"><a href="#">next</a></li>
       
             <?php else:?>
                <li><a href="?p=<?= urlencode($csv->next)?>">next</a></li>
        
        <?php endif;?>  
  </ul>
    <?php endif; ?>

<button><a href="../1-8.php">Add New Record</a></button>
</body>
</html>