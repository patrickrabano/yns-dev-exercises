<?php

class CsvPager{
	public $show = 20;
	public $range = 3;
	public $csv = array();
	public $pager = array();
	public $allPage = 0;
	public $page = 0;
    public $current = 1;

    private $source = array();
	public $prev = "";
	public $next = "";
	
	public function __construct($source, $param = 1, $show = 20, $range = 3){
		$this->source = $source;
		$this->show = $show;
		$this->range = $range;
		$this->allPage = count($this->source);
		if(is_numeric($param) && $param > 1){
			$this->current = $param;
		}
		//Round fractions up
		$this->page = ceil($this->allPage / $this->show);
		$showIndex = ($this->current - 1) * $this->show;
		while($showIndex < $this->current * $this->show){
			if($showIndex >= $this->allPage){
				break;
			}
			$this->csv[] = $this->source[$showIndex];
			$showIndex++;
		}
		if($this->current > 1){
			$this->prev = ($this->current - 1);
		}
		$start = $this->current - $this->range;
		if($start <= 0){
			$start = 1;
		}
		$end = $this->current + $this->range;
		if($end > $this->page){
			$end = $this->page;
		}
		for($pageNumber = $start; $pageNumber <= $end; $pageNumber++){
			$this->pager[] = $pageNumber;
		}
		if($this->current < $this->page){
			$this->next = ($this->current + 1);
		}
	}
}