<div class="posts form">	
<?php echo $this->Form->create('Post', ['enctype' => 'multipart/form-data']); ?>
	<div style="position: absolute;
	float:left;
	top: 70px;
    padding: 10px; margin-right:30px;">
	<fieldset>
		<legend><?php echo __('Edit Post'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('title', ['style'=>'width:300px; height:20px;']);
		echo $this->Form->input('body', ['style'=>'width:300px; height:60px;']); ?>
	</fieldset>
	</div>
	<div style="float:right; border: 3px solid black; padding: 10px; ">

<?php	foreach($post['Image'] as $images){
	
		echo '<div style="display:inline-block;">';
		echo $this->Html->image('/uploads/'. h($images['filename']), ['height' => '100', 'width' => '100']);
	
		echo $this->Form->checkbox('edit_blog_pic',
		 [
			'hiddenField' => false,
			'value' => $images['filename'],
			'name' => 'data[Post][edit_blog_pic][]'
		]);

		echo '</div>';	
	}
		
		echo $this->Form->input(
			'data[Post][edit_blog_pic][]', 
				[
				'type' => 'hidden', 
				'value' => '0',
				'name' => 'data[Post][edit_blog_pic][]'
				]);

		echo $this->Form->input('post_pic', ['type' => 'file' , 'multiple' => 'multiple' , 'name' => 'data[Post][post_pic][]']);
	?>
</div>
	<div style="padding-top: 90px;">
		<?php echo $this->Form->end(__('Submit')); ?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Home'), ['action' => 'index']); ?></li>
		<li><?php echo $this->Html->link(__('Add New Post'), ['action' => 'add']); ?></li>
		<li><?php echo $this->Form->postLink(__('Delete Post'),
		 	['action' => 'delete',$this->Form->value('Post.id')], 
		  	['confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('Post.id'))]); ?></li>		
	</ul>
</div>
