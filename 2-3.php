
<script>
function myFunction() {
        try {
            var val1 = parseInt(validate(document.getElementById("val1").value.trim()));
            var val2 = parseInt(validate(document.getElementById("val2").value.trim()));
            var operator = document.getElementById('operator').value;
            document.getElementById("demo").value = operate(val1, val2, operator);

        } catch (err) {
            alert("There was a problem: " + err.messaqge);
    }
}

function operate(val1, val2, operator) {
    if (operator == 'plus') {
        return val1 + val2;
    } 
    else if (operator == 'minus') {
        return val1 - val2;
    } 
    else if (operator == 'mult') {
        return val1 * val2;
    }
     else if (operator == 'div') {
        return val1 / val2;
    }
}

function validate(value) {
    if (value == null || value == "") {
        alert("Required Field");
        return 0;
    } else if (isNaN(value)) {
        alert("Must be a Number Field");
        return 0;
    } else return value;
}

</script>

Value 1:
<br>
<input type="text" id="val1">
<br>
Value 2:
<br>
<input type="text" id="val2">
<br>
    <select id="operator">
        <option value="plus">+</option>
        <option value="minus">-</option>
        <option value="mult">*</option>
        <option value="div">/</option>
    </select>
<button onclick="myFunction()">Solve</button> 

<br>
<input type="text" id="demo" name="demo" disabled="true">


