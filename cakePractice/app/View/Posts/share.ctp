<?php 
$logId = AuthComponent::user('id');

?>
<div class="posts view">

<fieldset>
		<legend><?='Share Post'?></legend>
		<?= $this->Form->create('Post'); ?>
	<?php
		echo $this->Form->input('title', ['style'=>'width:400px; height:20px;']);
        echo $this->Form->input('body' , ['style'=>'width:400px; height:80px;']);
        echo $this->Form->end(__('Submit'));
    ?>
	</fieldset>



<h2><?= 'Post' ?></h2>
	<dl>
		<dt><?= 'Posted By' ?></dt>
		<dd>
			<?= h(ucfirst($post['User']['first_name']) . " " . h($post['User']['last_name'])); ?>
			&nbsp;
		</dd>
		<dt><?= __('Title'); ?></dt>
		<dd>
			<?= h($post['Post']['title']); ?>
			&nbsp;
		</dd>
		<dt><?= __('Body'); ?></dt>
		<dd>
			<?= h($post['Post']['body']); ?>
			&nbsp;
		</dd>
		<dt><?= __('Created'); ?></dt>
		<dd>
			<?= h($post['Post']['created']); ?>
			&nbsp;
		</dd>
		<dt><?= __('Modified'); ?></dt>
		<dd>
			<?= h($post['Post']['modified']); ?>
			&nbsp;
		</dd>
	<br>


	</dl>



	</div>



<div class="actions">
	<h3><?= __('Actions'); ?></h3>
	<ul>
		<li><?= $this->Html->link(__('Home'), ['action' => 'index']); ?> </li>
			<?php if($logId === $post['User']['id']): ?>
				<li><?= $this->Html->link(__('Edit Post'), array('action' => 'edit', $post['Post']['id'])); ?> </li>
				<li><?= $this->Form->postLink(__('Delete Post'), array('action' => 'delete', $post['Post']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $post['Post']['id']))); ?> </li>		
			<?php endif; ?>
		<li><?= $this->Html->link(__('Add new post'), array('action' => 'add')); ?> </li>
	</ul>
</div>
