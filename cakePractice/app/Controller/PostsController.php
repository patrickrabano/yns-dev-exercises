<?php
App::uses('AppController', 'Controller');
App::uses('CakeTime', 'Utility');
/**
 * Posts Controller
 *
 * @property Post $Post
 * @property PaginatorComponent $Paginator
 */
class PostsController extends AppController {
/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');
/**
 * index method
 *
 * @return void
 */	
	public function search(){

		if(!empty($this->request->is('post'))){
			$cond=[];
			$cond['User.first_name LIKE'] = "%" . trim($this->request->data['User']['keyword']) . "%";
			$cond['User.last_name LIKE'] = "%" . trim($this->request->data['User']['keyword']) . "%";
			$cond['User.email LIKE'] = "%" . trim($this->request->data['User']['keyword']) . "%";
			$conditions['OR'] = $cond;
			$this->request->params['named']['User.keyword'] = $this->request->data['User']['keyword'];
		  
		}else{
			$conditions = "";
		}

		  
		$this->paginate = ['conditions' => $conditions ,'limit' => '10'];
		$result = $this->paginate('User');
		$this->set(['data'=> $result , 'user' => $this->Auth->user()]);
		
	}

	public function index() {
		
		$this->Post->recursive = 0;
		$this->set(['posts' => $this->Paginator->paginate() , 'user' => $this->Auth->user()]);


	}

	public function followers($id){
		
		$this->loadModel('Follow');
		$this->loadModel('User');
		$this->User->id = $id;
	
		$follow = $this->Follow->findAllByUserId($id);
		$following = $this->Follow->findAllByFollowUserId($id);

			$this->set(['followers'=> $follow , 'following'=> $following , 'user' => $this->User->findById($id)]);
		

			
	}

	public function unfollowUser($id = null , $userId = null){

		$conditions = ['user_id' => $userId , 'follow_user_id' => $id];

			$this->loadModel('Follow');
			$this->Follow->updateAll(['status' => 0], ['user_id' => $userId , 'follow_user_id' => $id]);
			return $this->redirect(['controller'=> 'users' ,'action' => 'view', $userId]);
				
	}
	


	public function followUser($id = null , $userId = null){

		$conditions = ['user_id' => $userId , 'follow_user_id' => $id , 'status' => 1];
		$conditions2 = ['user_id' => $userId , 'follow_user_id' => $id , 'status' => 0];
		$currentDate = date('Y-m-d H:i:s');

		$this->loadModel('Follow');
		$this->Follow->id = $id;
			
			if ($this->Follow->hasAny($conditions) || $this->Follow->hasAny($conditions2) ){
				
				$this->Flash->success(__('You are Following This User'));
				$this->Follow->updateAll(['status' => 1], ['user_id' => $userId , 'follow_user_id' => $id]);
				return $this->redirect(['controller'=> 'users' ,'action' => 'view', $userId]);
				
			}

		$this->Follow->saveAll(['user_id'=> $userId , 'follow_user_id' => $id , 'date_followed' => $currentDate]);
		$this->Flash->success(__('You are Following This User'));
		return $this->redirect(['controller'=> 'users' ,'action' => 'view', $userId]);		 
			
	}

	public function share($id = null){

		$post = $this->Post->findById($id);
		$this->set(['post' => $post]);

		$userId = $this->Auth->user('id');
		$this->request->data['Post']['share_post_id'] = $id;
		$this->request->data['Post']['user_id'] = $userId;


		if($this->request->is('post')){
		
			$this->Post->save($this->request->data);
			$this->Flash->success(__('You Shared This Post!'));
			$this->redirect(['action' => 'index']);	

		}


	}

	public function like($id = null, $userId = null){

		$conditions = ['post_id' => $id , 'user_id' => $userId , 'deleted' => 1];
		$conditions2 = ['post_id' => $id , 'user_id' => $userId , 'deleted' => 0];

		$this->loadModel('Like');
		

		$this->request->data['Like']['post_id'] = $id;
		$this->request->data['Like']['user_id'] = $userId;

		if($this->Like->hasAny($conditions) || $this->Like->hasAny($conditions2)){
			
			$this->Flash->success(__('You Like This Post!'));
			$this->Like->updateAll(['deleted' => 0], ['post_id' => $id , 'user_id' => $userId]);
			return $this->redirect(['action' => 'view', $id]);

		}

			$this->Like->saveAll(['post_id'=> $id , 'user_id' => $userId]);
			$this->Flash->success(__('You Liked This Post!'));
			$this->redirect(['action' => 'view', $id]);	

	}


	public function unlike($id, $userId){
		$this->loadModel('Like');

			$this->Flash->success('You Unlike This Post!');
			$this->Like->updateAll(['deleted' => 1], ['post_id' => $id , 'user_id' => $userId]);
			return $this->redirect(['action' => 'view', $id]);
		
	
	}


/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 * 
 * 
 */

	public function editcomment($id){


		$this->loadModel('Comment');
		$this->Comment->id = $this->Comment->findById($id);
		$comment = $this->Comment->findById($id);

		
		if($this->request->is('post') || $this->request->is('put')){

		if($this->Auth->user('id') == $comment['Comment']['user_id']){

			if($this->Comment->save($this->request->data)){

				$this->Flash->success(__('Comment Updated!'));
				$this->redirect(['action' => 'view', $comment['Comment']['post_id']]);

			}	

		}

		$this->Flash->Error(__('You cannot edit comment'));
		$this->redirect(['action' => 'view', $comment['Comment']['post_id']]);

	}


}

	public function deletecomment($id){
		
		$this->loadModel('Comment');
		$comment = $this->Comment->findById($id);
		
		if($this->Auth->user('id') == $comment['Comment']['user_id']){

		$this->Flash->success('You Deleted your comment!');

		$this->Comment->updateAll(['Comment.deleted' => 1], ['Comment.id' => $id]);

		return $this->redirect(['action' => 'view', $comment['Comment']['post_id']]);

		}

		$this->Flash->Error(__('You cannot delete comment'));
		$this->redirect(['action' => 'view', $comment['Comment']['post_id']]);
		
	}
	
	public function view($id = null) {
		if (!$this->Post->exists($id)) {

			throw new NotFoundException(__('Invalid post'));
		}
		$post = $this->Post->findById($id);

		$sharePostId = $this->Post->findById($post['Post']['share_post_id']);

		$this->set(['post' => $post , 'share' => $sharePostId]);
		
		if($this->request->is('post')){
			array_map([$this, 'loadModel'] , ['Comment' , 'User']);

			$logId = $this->Auth->user('id');
			$user = $this->User->findById($logId);

			$this->Comment->create();
			$this->request->data['Comment']['post_id'] = $id;
			$this->request->data['Comment']['user_id'] = $logId;
			$this->request->data['Comment']['username'] = $user['User']['username'];
			

			if($this->Comment->save($this->request->data)){

				$this->Flash->success(__('Comment Saved!'));
				$this->redirect(['action' => 'view', $id]);

			}	

		}

		$options = array('conditions' => array('Post.' . $this->Post->primaryKey => $id));
		$this->set('post', $this->Post->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
		public function add() {

			if ($this->request->is('post')) {
				
				$imageFiles = $this->request->data['Post']['post_pic'];

				$this->request->data['Post']['user_id'] = $this->Auth->user('id');
				
				if ($this->Post->save($this->request->data)) {
					

					foreach($imageFiles as $files){

						$postId = $this->Post->getInsertID();

						$this->loadModel('Image');
						
							$name = $files['name'];
							$fileName = md5(Security::hash($files['tmp_name'].Security::randomBytes(32)));
							$location = 'uploads/' . $fileName;
							$extension = explode(".", $name);	
							$fileType =  strtolower(end($extension));
							$newFileName = $fileName .'.'. $fileType;
	
							$this->request->data['Image']['filename'] = $fileName;
							$this->request->data['Image']['post_id'] = $postId;

							move_uploaded_file($files['tmp_name'], WWW_ROOT . $location . "." . $fileType);
							$this->Image->saveAll(['post_id'=> $postId , 'filename' => $fileName.".".$fileType]);
								
							}
							
							$this->Flash->success(__('Your post has been saved.'));
							return $this->redirect(array('action' => 'index'));
							
						}
							
					}
						
				}
			
		
	

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {

		$post = $this->Post->findById($id);
		$this->set(['post' => $post]);

		if (!$this->Post->exists($id)) {
			throw new NotFoundException(__('Invalid post'));
		}


		if ($this->request->is(['post', 'put'])) {
			

			if ($this->Post->save($this->request->data)) {
				$this->loadModel('Image');

				$selectedFiles = $this->request->data['Post']['edit_blog_pic'];
		
				foreach($selectedFiles as $selected){

					$this->Image->updateAll(['deleted' => 1], ['filename' => $selected, 'post_id' => $id]);

				}

				$imageFiles = $this->request->data['Post']['post_pic'];
				foreach($imageFiles as $files){

					$this->loadModel('Image');
					
					$name = $files['name'];
					$fileName = md5(Security::hash($files['tmp_name'].Security::randomBytes(32)));
					$location = 'uploads/' . $fileName;
					$extension = explode(".", $name);	
					$fileType =  strtolower(end($extension));
					$newFileName = $fileName .'.'. $fileType;

					$this->request->data['Image']['filename'] = $fileName;
					$this->request->data['Image']['post_id'] = $id;

					move_uploaded_file($files['tmp_name'], WWW_ROOT . $location . "." . $fileType);
					$this->Image->saveAll(['post_id'=> $id , 'filename' => $fileName.".".$fileType]);
							
				}
				
				$this->Flash->success(__('The post has been saved.'));
				return $this->redirect(array('action' => 'index'));

			} else {
				$this->Flash->error(__('The post could not be saved. Please, try again.'));
			}

		} else {

			$options = array('conditions' => array('Post.' . $this->Post->primaryKey => $id));
			$this->request->data = $this->Post->find('first', $options);
	
		}

	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->Post->exists($id)) {
			throw new NotFoundException(__('Invalid post'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Post->delete($id)) {
			$this->Flash->success(__('The post has been deleted.'));
		} else {
			$this->Flash->error(__('The post could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Post->recursive = 0;
		$this->set('posts', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Post->exists($id)) {
			throw new NotFoundException(__('Invalid post'));
		}
		$options = array('conditions' => array('Post.' . $this->Post->primaryKey => $id));
		$this->set('post', $this->Post->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Post->create();
			if ($this->Post->save($this->request->data)) {
				$this->Flash->success(__('The post has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The post could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Post->exists($id)) {
			throw new NotFoundException(__('Invalid post'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Post->save($this->request->data)) {
				$this->Flash->success(__('The post has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The post could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Post.' . $this->Post->primaryKey => $id));
			$this->request->data = $this->Post->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->Post->exists($id)) {
			throw new NotFoundException(__('Invalid post'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Post->delete($id)) {
			$this->Flash->success(__('The post has been deleted.'));
		} else {
			$this->Flash->error(__('The post could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function isAuthorized($user) {
		 
		if ($this->action === 'add') {
			return true;
		}
	
		
		if (in_array($this->action, array('edit', 'delete'))) {
			$postId = (int) $this->request->params['pass'][0];
			if ($this->Post->isOwnedBy($postId, $user['id'])) {
				return true;
			}
			
		}
		
		return parent::isAuthorized($user);
	}
}
