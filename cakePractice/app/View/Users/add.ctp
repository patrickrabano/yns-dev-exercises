<div class="users form">
<?php echo $this->Form->create('User'); ?>
	<fieldset>
		<legend><?php echo __('Add User'); ?></legend>
	<?php
		echo $this->Form->input('email');
		echo $this->Form->input('first_name');
		echo $this->Form->input('last_name');
		echo $this->Form->input('gender' , ['options' => ['0' => 'Male' , '1' => 'Female']]);	
		echo $this->Form->input('username');
		echo $this->Form->input('password');
        echo $this->Form->input('confirm_password', ['type' => 'password']);
        ?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Log-in'), array('action' => 'login')); ?></li>
	</ul>
</div>
