<?php

App::uses('AppModel', 'Model');

class Follow extends AppModel {

    public $belongsTo = [
      
      'UserFrom'=>[
         'className'=>'User',
         'foreignKey'=>'user_id'
        ],

      'UserTo'=> [
         'className'=>'User',
         'foreignKey'=>'follow_user_id'
      ],

    ];

}