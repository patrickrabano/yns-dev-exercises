<div class="posts form">
	<fieldset>
		<legend><?php echo __('Add Post'); ?></legend>
		<?php echo $this->Form->create('Post', array('enctype' => 'multipart/form-data')); ?>
	<?php
		echo $this->Form->input('title');
		echo $this->Form->input('body');
		echo $this->Form->input('post_pic', ['type' => 'file' , 'multiple' => 'multiple' , 'name' => 'data[Post][post_pic][]']);
		
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Posts'), array('action' => 'index')); ?></li>
	</ul>
</div>
