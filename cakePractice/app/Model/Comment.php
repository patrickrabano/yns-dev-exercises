<?php

App::uses('AppModel', 'Model');

class Comment extends AppModel {

    public $belongsTo = [
        'UserFrom' => [
            'className' => 'User',
            'foreignKey' => 'user_id'
            ],
            
            'Post' => [
                'className' => 'Post',
                'foreignKey' => 'post_id'
                ],
			
		];

}