<html>

<style>

#wrapper {
    overflow: hidden;
	
}
#c1 {
    float:left;
	width:650px; border: 3px solid black; padding: 10px; margin-bottom:25px;
    
}
#c2 {
	
	position: absolute;
    top: 110px;
    right: 40px;
	border: 3px solid black; padding: 10px; margin-right:30px;
}

</style>

<div class="users view">
<?php 

$logId = AuthComponent::user('id');



?>

<h2><?= h(ucfirst($user['User']['first_name'])). "'s " . "Posts" ?></h2>
	
	<?php foreach($user['posts'] as $posts => $allPosts): ?>
	
<div id="wrapper">
<div id="c1">

	<dl style="width: 50%; float:left !important;">
		<dt><?= 'Title:' ?></dt>
		<dd>
			<?= $this->Html->link(__(h($allPosts['title'])), ['controller'=>'posts','action' => 'view', $allPosts['id']]); ?>
			&nbsp;
		</dd>
		<dt><?= __('Date Posted:'); ?></dt>
		<dd>
			<?= h($user['User']['created']); ?>
			&nbsp;
		</dd>
		<dt><?= __('Modified:'); ?></dt>
		<dd>
			<?= h($user['User']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?= "<br>" ?></dt>

		<div class="actions">
		
			<li><?= $this->Html->link(__('Like Post'), 
			[
				
				'controller'=>'posts',
				'action' => 'like' , $allPosts['id'],$logId
			
			]);?> 
			</li>
		
		</div>
		
		<div class="actions">
		
		<li style="margin-left: 30px;"><?= $this->Html->link(__('Comment Post'), 
		[
			'controller'=> 'posts',
			'action' => 'view' , $allPosts['id']
		
		]); ?>
		</li>
	
		</div>

		<div class="actions">
		
		<li style="margin-left: 100px;"><?= $this->Html->link(__('Share Post'),
		[
			 'controller'=>'posts',
			 'action' => 'share' , $allPosts['id']
		
		]);?> 
		
		</li>
	
		</div>
	
	
	</dl>

	<dl style="width: 49%; float:right !important;">
	<dt><?= 'Comments:' ?></dt>
	<?php	foreach($user['comments'] as $comments => $allComments):
				if($allPosts['id'] == $allComments['post_id']): ?>
				
		
		<dd>
			<?= "<br>" ?>
		</dd><br>
	
		<dd>
			<?= h($allComments['message']) . " By " . h($allComments['username']) . '<br>'?>
		</dd>

		<?php  endif; endforeach; ?>
	</dl>
		


</div>

</div>
	<?php endforeach; ?>
	
		<div id="c2">
		
			<h3><?= h(ucfirst($user['User']['first_name'])). "'s " . "Followers" ?></h3>
				<?php foreach($user['userFollow'] as $followers): ?>				
					<div style="text-align:center;">			
						<?php if($followers['Follow']['status'] == 1): ?>
							<?= $this->Html->image('/uploads/' . h($followers['image']), ['height' => '80px' , 'width' => '90px']) ?>
							<h4><?= $this->Html->link(__(h($followers['first_name'] . " " . $followers['last_name'])), ['controller'=>'users','action' => 'view', $followers['id']]) ?></h4>		
						<?php endif; ?>
					</div>  
				<?php endforeach; ?>

			<h3><?= h(ucfirst($user['User']['first_name'])). "'s " . "Following" ?></h3>
			<?php foreach($user['userFollowing'] as $following): ?>
				<div style="text-align:center;">
					<?php if($following['Follow']['status'] == 1): ?>
					<?= $this->Html->image('/uploads/' . h($following['image']), ['height' => '80px' , 'width' => '90px']) ?>
					<h4><?= $this->Html->link(__(h($following['first_name'] . " " . $following['last_name'])), ['controller'=>'users','action' => 'view', $following['id']]) ?></h4>		

					<?php endif; ?>
				</div> 
			<?php endforeach; ?>
		</div>	
	</div>

<div class="actions">
	<h3 style="text-align:center;"><?= h(ucfirst($user['User']['first_name'])) . " " .  h(ucfirst($user['User']['last_name'])) ?></h3>
	<div style="margin-left: 15px;">
	<?= $this->Html->image('/uploads/' . h($user['User']['image']), ['height' => '150px' , 'width' => '180px']) ?>
	</div>
	<ul>

		<?php if($logId !== $user['User']['id']): ?>

			<?php foreach($user['userFollow'] as $details):

					if($logId === $details['id'] && $details['Follow']['status'] == 1): ?>

						<li><?= $this->Html->link(__('Unfollow User'), array('controller'=>'Posts','action' => 'unfollowUser', $logId , $user['User']['id'])); ?></li>
						
					<?php endif; endforeach; ?>

			<li><?= $this->Html->link(__('Follow User'), array('controller'=>'Posts','action' => 'followUser', $logId , $user['User']['id'])); ?> </li>
			
		<?php endif; ?>
		<li><?= $this->Html->link(__(h('View ' . ucfirst($user['User']['first_name'])) . "'s " . "Followers"), 
		[
			'controller'=>'posts',
			'action' => 'followers',$user['User']['id']		
		]); 
		?> 
		</li>				
		<li><?= $this->Html->link(__('Home'), array('controller'=>'Posts','action' => 'index')); ?> </li>
		<li><?= $this->Html->link(__('Add new post'), array('controller'=>'Posts','action' => 'add')); ?> </li>
		<li><?= $this->Html->link(__('Edit Your Profile'), array('controller' => 'users', 'action' => 'edit', $logId)); ?></li>
		<li><?= $this->Html->link(__('Logout'), array('controller' => 'users', 'action' => 'logout')); ?></li>
	</ul>
</div>
</html>